package pl.microservice.emailSender.message.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.microservice.emailSender.message.model.Email;

public interface SenderRepository extends JpaRepository<Email, Long> {
}

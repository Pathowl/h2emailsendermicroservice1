package pl.microservice.emailSender.message.controller;

import org.springframework.web.bind.annotation.*;
import pl.microservice.emailSender.message.model.Email;
import pl.microservice.emailSender.message.model.dto.EmailDto;
import pl.microservice.emailSender.message.service.EmailService;
import pl.microservice.emailSender.message.service.SenderService;

import java.util.List;


@RestController
@RequestMapping("/api")
public class SenderController {

    private final SenderService senderService;
    private final EmailService emailService;

    public SenderController(SenderService senderService, EmailService emailService) {
        this.senderService = senderService;
        this.emailService = emailService;
    }


    @PostMapping("/send")
    public void sendEmails(@RequestBody EmailDto emailDto){
        String[] emailAddressesArray = senderService.getReceiverAddresses().stream()
                .map(Email::getReceiverAddress)
                .toArray(String[]::new);
        emailService.send(emailAddressesArray, emailDto.subject(), emailDto.message());

    }



    @PostMapping()
    public Email saveReceiverAddress(@RequestBody EmailDto emailDto){
        return senderService.saveReceiverAddress(emailDto);
    }

    @GetMapping()
    public List<Email> getReceiverAddresses(){
        return senderService.getReceiverAddresses();
    }

    @PutMapping("/update/{id}")
    public Email updateReceiverAddresses(@RequestBody EmailDto emailDto, @PathVariable Long id){
        return senderService.updateReceiverAddresses(emailDto,id);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteReceiverAddresses(@PathVariable Long id){
        senderService.deleteReceiverAddresses(id);
    }


}

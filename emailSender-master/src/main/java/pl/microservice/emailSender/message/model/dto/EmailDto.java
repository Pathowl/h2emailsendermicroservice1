package pl.microservice.emailSender.message.model.dto;



public record EmailDto(String receiverAddress, String subject, String message) {
}

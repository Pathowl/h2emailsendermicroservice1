package pl.microservice.emailSender.message.service;

import org.springframework.stereotype.Service;
import pl.microservice.emailSender.message.model.Email;
import pl.microservice.emailSender.message.model.dto.EmailDto;
import pl.microservice.emailSender.message.repository.SenderRepository;

import java.util.List;


@Service
public class SenderService {

    private final SenderRepository senderRepository;

    public SenderService(SenderRepository senderRepository) {
        this.senderRepository = senderRepository;
    }

    public Email saveReceiverAddress(EmailDto emailDto) {
        return senderRepository.save(Email.builder()
                        .receiverAddress(emailDto.receiverAddress())
                        .build());
    }

    public List<Email> getReceiverAddresses() {
        return senderRepository.findAll();
    }

    public Email updateReceiverAddresses(EmailDto emailDto, Long id) {
        return senderRepository.save(Email.builder()
                        .id(id)
                        .receiverAddress(emailDto.receiverAddress())
                .build());
    }

    public void deleteReceiverAddresses(Long id) {
        senderRepository.deleteById(id);
    }


}
